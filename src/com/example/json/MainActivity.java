package com.example.json;

import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		try {
			//Leemos nuestro objetoJson
		     JSONObject jsonObject = new JSONObject(" { \"arrayColores\":[{ \"nombreColor\":\"rojo\", \"valorHexadec\":\"#f00\" }, { \"nombreColor\":\"verde\", \"valorHexadec\":\"#0f0\" }, { \"nombreColor\":\"azul\", \"valorHexadec\":\"#00f\" }, { \"nombreColor\":\"cyan\", \"valorHexadec\":\"#0ff\" }, { \"nombreColor\":\"magenta\", \"valorHexadec\":\"#f0f\" }, { \"nombreColor\":\"amarillo\", \"valorHexadec\":\"#ff0\" }, { \"nombreColor\":\"negro\", \"valorHexadec\":\"#000\" } ] }");
		     //Obtenemos la clave que contiene el array
		     JSONArray colores = (JSONArray) jsonObject.getJSONArray("arrayColores");
		     String res = "Color \t\t ValorHex \n";
		     for (int i = 0; i < colores.length(); ++i) {
		    	    JSONObject color = colores.getJSONObject(i);
		    	    res += color.get("nombreColor") + "\t\t" + color.get("valorHexadec") + "\n";
		    }
		     ((TextView) findViewById(R.id.textView1)).setText(res);
		     
		}catch (JSONException err){
		     Log.d("Error", err.toString());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
